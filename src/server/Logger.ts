import * as winston from 'winston';

const logger = new winston.Logger({
  level: 'debug',
  transports: [
    new winston.transports.Console({
      console: {
        colorize: true,
        handleExceptions: true,
        json: true,
        level: 'debug',
      },
    }),
  ],
});

export default logger;
