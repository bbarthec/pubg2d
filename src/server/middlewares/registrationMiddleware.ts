import { Request, Response } from 'express';

import { registerNewPlayer } from '../game-session';
import Logger from '../Logger';

export default (req: Request, res: Response) => {
  Logger.info('New registration', req.body);
  registerNewPlayer(req.body.playerName)
    .then(() => res.status(200).send())
    .catch((error: Error) => res.status(409).send(error.message));
};
