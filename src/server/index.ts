/* tslint:disable:no-console */
import config from 'config';
import express from 'express';
import http from 'http';
import httpProxyMiddleware from 'http-proxy-middleware';
import path from 'path';
import socketIO from 'socket.io';

import { registrationMiddleware } from './middlewares';
import newConnectionHandler from './socket/newConnectionHandler';

const host = config.get('server.host');
const port = config.get('server.port');
const webpackPort = config.get('server.webpack.dev.port');

const app = express();
const server = new http.Server(app);
const io = socketIO(server, {
  cookie: false,
  pingInterval: 10000,
  pingTimeout: 5000,
  serveClient: false,
});

io.on('connection', newConnectionHandler);

app.use(express.json());
app.use('/registration', registrationMiddleware);

if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static(path.resolve(__dirname, '../../public'), { index: false }));
  app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, '../../public/index.html')));
} else {
  app.use('/', httpProxyMiddleware({ target: `http://${host}:${webpackPort}` }));
}

server.listen(port, () => {
  console.info('==> ✅  Server is listening');
  console.info(`==> 🌎  Go to http://${host}:${port}`);
});
