import config from 'config';
import _ from 'lodash';

import Logger from '../Logger';
import Player from './Player';

const players = new Map<string, Player>();
const reservedNames = new Set<string>();

const gameConfig = {
  gameHeight: config.get('server.gameHeight') as number,
  gameWidth: config.get('server.gameWidth') as number,
  lobbyTime: config.get('server.lobby') as number,
};

const currentSession = {
  countdown: gameConfig.lobbyTime,
  lobbyTime: gameConfig.lobbyTime,
  playersNumber: () => players.size,
  startedCountdown: false,
};

export function registerNewPlayer(playerName: string) {
  return new Promise((resolve, reject) => {
    if (currentSession.startedCountdown && currentSession.countdown <= 5) {
      reject(new Error('Game already started. Wait for it to end'));
    } else if (players.has(playerName) || reservedNames.has(playerName)) {
      reject(new Error('Name already taken!'));
    } else {
      reservedNames.add(playerName);
      setTimeout(
        () => reservedNames.delete(playerName),
        2000);
      resolve();
    }
  });
}

export function createNewPlayer(socket: SocketIO.Socket, { playerName }: { playerName: string }) {
  const player = new Player(socket, {
    name: playerName,
    rotation: 2 * Math.PI * Math.random(),
    x: Math.random() * (gameConfig.gameWidth - 400) + 200,
    y: Math.random() * (gameConfig.gameHeight - 400) + 200,
  });

  reservedNames.delete(playerName);

  socket.on('disconnect', () => {
    players.delete(playerName);
    Logger.debug('socket disconnection', players.size);
  });

  socket.on('move', (data) => {
    socket.broadcast.emit('enemy move', data);
  });

  socket.on('bullet', (data) => {
    socket.broadcast.emit('enemy bullet', data);
  });

  socket.on('hit', ({ name }: { name: string }) => {
    const targetPlayer = players.get(name);
    if (!targetPlayer) {
      return;
    }
    targetPlayer.health -= 10;
    if (targetPlayer.health <= 0) {
      targetPlayer.socket.emit('game over', { position: players.size });
      players.delete(name);
    }
    Logger.debug('hit', {
      targetHealth: targetPlayer.health,
      targetName: targetPlayer.name,
    });
    socket.emit('hit', {
      health: targetPlayer.health,
      name: targetPlayer.name,
    });
    socket.broadcast.emit('hit', {
      health: targetPlayer.health,
      name: targetPlayer.name,
    });
    if (players.size === 1) {
      socket.emit('game over', { position: 1 });
      players.clear();
      reservedNames.clear();
      socket.broadcast.disconnect();
      socket.disconnect();
      resetGame();
    }
  });

  socket.emit('config', {
    playerRotation: player.rotation,
    playerX: player.x,
    playerY: player.y,
    players: Array.from(players.values()).map(({ x, y, name, rotation }) => ({ x, y, name, rotation })),
    worldHeight: gameConfig.gameHeight,
    worldWidth: gameConfig.gameWidth,
  });
  players.set(playerName, player);
  socket.broadcast.emit('new player', {
    name: player.name,
    rotation: player.rotation,
    x: player.x,
    y: player.y,
  });

  startLobbyCountdown();
}

function resetGame() {
  currentSession.countdown = gameConfig.lobbyTime;
  currentSession.lobbyTime = gameConfig.lobbyTime;
  currentSession.startedCountdown = false;
}

function startLobbyCountdown() {
  Logger.debug('Requested Lobby Countdown with players number:', currentSession.playersNumber());
  if (currentSession.startedCountdown || currentSession.playersNumber() < 2) {
    return;
  }
  currentSession.startedCountdown = true;
  setTimeout(
    lobbyCountdown,
    1000);
}

function lobbyCountdown() {
  if (currentSession.countdown === 0) {
    Logger.debug('Starting game!');
    players.forEach(player => player.socket.emit('game start'));
    return;
  }
  Logger.debug('Lobby Countdown:', currentSession.countdown);
  players.forEach(player => player.socket.emit('lobby time', { time: currentSession.countdown }));
  currentSession.countdown -= 1;
  setTimeout(lobbyCountdown, 1000);
}
