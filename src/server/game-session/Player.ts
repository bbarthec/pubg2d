interface PlayerConfig {
  name: string;
  rotation: number;
  x: number;
  y: number;
}

export default class Player {
  public name: string;
  public x: number;
  public y: number;
  public rotation: number;
  public socket: SocketIO.Socket;
  public health = 100;

  constructor(socket: SocketIO.Socket, config: PlayerConfig) {
    this.name = config.name;
    this.x = config.x;
    this.y = config.y;
    this.rotation = config.rotation;
    this.socket = socket;
  }
}
