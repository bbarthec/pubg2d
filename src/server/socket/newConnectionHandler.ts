import { createNewPlayer } from '../game-session';
import Logger from '../Logger';

export default (socket: SocketIO.Socket) => {
  Logger.info('socket connected!');
  socket.on('player request', (data: { playerName: string }) => createNewPlayer(socket, data));
};
