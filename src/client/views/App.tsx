import React from 'react';
import { Route, Switch } from 'react-router';
import { Link } from 'react-router-dom';

import MainView from './MainView';

import './App.scss';

export default class App extends React.Component<{}, {}> {
  public render() {
    return (
      <div className='App'>
        <Switch>
          <Route exact path='/' component={MainView} />
        </Switch>
      </div>
    );
  }
}
