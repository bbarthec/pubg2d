import axios, { AxiosError } from 'axios';
import React, { ChangeEvent, FormEvent, MouseEvent } from 'react';

import Spinner from './Spinner';

import BackgroundImage from '@client/assets/images/background.png';

import './RegistrationForm.scss';

interface Props {
  onSuccessRegistration: (playerName: string) => void;
}

interface State {
  loading: boolean;
  playerName: string;
  error: any;
}

export default class RegistrationForm extends React.PureComponent<Props, State> {
  public state = {
    error: undefined,
    loading: false,
    playerName: localStorage.getItem('playerName') || '',
  };

  private inputRef: HTMLInputElement | null;

  public componentDidMount() {
    if (this.inputRef) {
      this.inputRef.focus();
    }
  }

  public render() {
    const { error, loading, playerName } = this.state;

    return (
      <div className='RegistrationForm'>
        <div className='RegistrationForm-background' />
        <form
          className='RegistrationForm-form'
        >
          { error && (
            <div className='RegistrationForm-error'>
              {error}
            </div>
          )}
          <input
            type='siubmit'
            onSubmit={this.handleSubmit}
            className='RegistrationForm-input'
            value={playerName}
            onChange={this.handleChange}
            onFocus={() => this.setState({ error: undefined })}
            placeholder='Enter you name here'
            ref={r => (this.inputRef = r)}
          />
          <button
            className='RegistrationForm-button'
            onClick={this.handleSubmit}
            disabled={loading || !playerName || error}
          >
            {loading ? (
              <Spinner />
            ) : (
              'Enter the game!'
            )}
          </button>
        </form>
      </div>
    );
  }

  private handleSubmit = (
    e: MouseEvent<HTMLButtonElement> | FormEvent<HTMLFormElement> | FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    const { playerName, loading } = this.state;
    if (loading) {
      return;
    }
    this.setState({ loading: true }, () => {
      axios.post('/registration', { playerName })
        .then(() => this.props.onSuccessRegistration(playerName))
        .catch((error: AxiosError) =>
          this.setState({ error: error.response && error.response.data || error.message, loading: false }));
    });
  }

  private handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    this.setState({ playerName: e.target.value });
  }
}
