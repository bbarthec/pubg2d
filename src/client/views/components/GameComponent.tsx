import React from 'react';

import Game from '@client/game';

import './GameComponent.scss';

interface Props {
  playerName: string;
  handleRestart: () => void;
}

export default class GameComponent extends React.PureComponent<Props, {}> {
  private game: Game;

  public componentDidMount() {
    this.game = new Game(this.props.playerName, this.props.handleRestart);
    window.addEventListener('resize', this.handleResize);
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  public render() {
    return (
      <div
        className='GameComponent'
        id='game'
      />
    );
  }

  private handleResize() {
    if (this.game) {
      this.game.resize(window.innerWidth, window.innerHeight);
    }
  }
}
