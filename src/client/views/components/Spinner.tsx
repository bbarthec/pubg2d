import React from 'react';

import './Spinner.scss';

export default () => (
  <div className='Spinner'>
    <div className='Spinner-rect1' />
    <div className='Spinner-rect2' />
    <div className='Spinner-rect3' />
    <div className='Spinner-rect4' />
    <div className='Spinner-rect5' />
  </div>
);
