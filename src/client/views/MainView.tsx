import React from 'react';

import {
  GameComponent,
  RegistrationForm,
} from './components';

import './MainView.scss';

export default class MainView extends React.PureComponent<{}, State> {
  public state = {
    player: {
      confirmed: false,
      name: undefined,
    },
  };

  public render() {
    return (
      <div className='MainView'>
        {this.renderRegistrationForm()}
        {this.renderGame()}
      </div>
    );
  }

  private handleSuccessRegistration = (name: string) => this.setState({ player: { name, confirmed: true } });

  private restartGame = () => window.location.reload();

  private renderRegistrationForm() {
    const { confirmed } = this.state.player;
    return !confirmed && (
      <RegistrationForm
        onSuccessRegistration={this.handleSuccessRegistration}
      />
    );
  }

  private renderGame() {
    const { confirmed } = this.state.player;
    return confirmed && (
      <GameComponent
        playerName={this.state.player.name!}
        handleRestart={this.restartGame}
      />
    );
  }
}

interface State {
  player: {
    name?: string;
    confirmed: boolean;
  };
}
