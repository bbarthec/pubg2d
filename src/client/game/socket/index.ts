import io from 'socket.io-client';

let socket: SocketIOClient.Socket;

export interface PlayerData {
  x: number;
  y: number;
  rotation: number;
  name: string;
}

export function initSocket(playerName: string, {
  onSessionRejected,
  onLobbyTick,
  onGameStart,
  onConfig,
  onNewPlayer,
}: {
  onSessionRejected: () => void,
  onConfig: ({}: {
    playerX: number,
    playerY: number,
    playerRotation: number,
    players: PlayerData[],
    worldWidth: number,
    worldHeight: number,
  }) => void,
  onLobbyTick: ({}: { time: number }) => void,
  onGameStart: () => void,
  onNewPlayer: (player: PlayerData) => void,
}) {
  socket = io();
  socket.on('session rejected', onSessionRejected);
  socket.on('lobby time', onLobbyTick);
  socket.on('game start', onGameStart);
  socket.on('config', onConfig);
  socket.on('new player', onNewPlayer);
  setTimeout(socket.emit('player request', { playerName }), 50);
  return socket;
}

export function getSocket() {
  return socket;
}
