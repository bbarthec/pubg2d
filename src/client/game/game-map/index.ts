import _ from 'lodash';

import {
  Crate,
  House,
  Rock,
  Shack,
  Tile,
  Tree,
} from './objects';

export default class GameMap {
  public static loadSprites(scene: Phaser.Scene) {
    // Shack.loadSprite(scene);
    Tile.loadSprite(scene);
    Tree.loadSprite(scene);
    House.loadSprite(scene);
    // Crate.loadSprite(scene);
    // Rock.loadSprite(scene);
  }

  private scene: Phaser.Scene;
  private objects: Phaser.GameObjects.GameObject[] = [];

  constructor(scene: Phaser.Scene) {
    this.scene = scene;
    const { x, y, width, height } = this.scene.physics.world.bounds;
    const background = this.scene.add.graphics();
    background.fillStyle(0x20A020, 0.3);
    background.fillRect(x, y, width, height);
    this.addGridTiles();
    this.addObstacles();
  }

  public addCollisionWith(object: Phaser.GameObjects.GameObject, collideCallback?: ArcadePhysicsCallback) {
    this.objects.forEach(obj => this.scene.physics.add.collider(obj, object, collideCallback));
  }

  private addGridTiles() {
    const { x, y, width, height } = this.scene.physics.world.bounds;
    const tileSize = 400;
    for (let c = 0; c < width; c += tileSize) {
      for (let r = 0; r < height; r += tileSize) {
        this.scene.add.existing(new Tile(this.scene, { x: c, y: r }));
      }
    }
  }

  private addObstacles() {
    const { width, height } = this.scene.physics.world.bounds;
    const trees = _.range(200, width - 200, 500)
      .map(x => ({ x, ys: _.range(200, width - 200, 500) }))
      .map(({ x, ys }) => ys.map(y => ({
        x: x + Math.sin(y * x + 2.21) * 200,
        y: y + Math.cos(y * x - 1.14) * 200,
      })))
      .reduce(
        (acc, current) => {
          acc.push(...current);
          return acc;
        },
        [],
      ).
      map(options => new Tree(this.scene, options));

    this.objects.push(...trees);

    const houses = [{ x: 600, y: 1100 }, { x: 2100, y: 2500 }, { x: 3200, y: 1200 }, { x: 2450, y: 100 }]
      .map(options => options.x < width && options.y < height && new House(this.scene, options))
      .filter(<T>(n: T | false): n is T => !!n);

    this.objects.push(...houses);
  }
}
