import sprite from '@client/assets/sprites/house.svg';

export default class House extends Phaser.GameObjects.Sprite {
  public static spriteName = 'HouseSprite';

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(House.spriteName, sprite);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, House.spriteName);
    this.scene.add.existing(this);
    this.setSize(420, 340);
    this.setOrigin(0, 0);
    this.setDisplaySize(420, 340);
    this.scene.physics.world.enable(this, Phaser.Physics.Arcade.STATIC_BODY);
  }
}
