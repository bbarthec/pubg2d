import sprite from '@client/assets/sprites/tree.svg';

export default class Tree extends Phaser.GameObjects.Image {
  public static spriteName = 'TreeSprite';

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Tree.spriteName, sprite);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Tree.spriteName);
    this.scene.add.existing(this);
    this.setSize(50, 50);
    this.setOrigin(0.5, 0.5);
    this.setDisplaySize(256, 256);
    this.setDepth(1);
    this.scene.physics.world.enable(this, Phaser.Physics.Arcade.STATIC_BODY);
    (this.body as Phaser.Physics.Arcade.Body).setSize(50, 50, false);
    (this.body as Phaser.Physics.Arcade.Body).setOffset(103, 103);
  }
}
