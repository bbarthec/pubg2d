import sprite from '@client/assets/sprites/crate.svg';

export default class Crate extends Phaser.GameObjects.Sprite {
  public static spriteName = 'CrateSprite';
  public static spriteAsset = sprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Crate.spriteName, Crate.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Crate.spriteName);
    this.setSize(420, 340);
    this.setDisplaySize(420, 340);
  }
}
