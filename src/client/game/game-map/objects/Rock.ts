import sprite from '@client/assets/sprites/rock.svg';

export default class Rock extends Phaser.GameObjects.Sprite {
  public static spriteName = 'RockSprite';
  public static spriteAsset = sprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Rock.spriteName, Rock.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Rock.spriteName);
    this.setSize(420, 340);
    this.setDisplaySize(420, 340);
  }
}
