import sprite from '@client/assets/sprites/shack.svg';

export default class Shack extends Phaser.GameObjects.Sprite {
  public static spriteName = 'ShackSprite';
  public static spriteAsset = sprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Shack.spriteName, Shack.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Shack.spriteName);
    this.setSize(420, 340);
    this.setDisplaySize(420, 340);
  }
}
