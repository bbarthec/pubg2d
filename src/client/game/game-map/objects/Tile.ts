import sprite from '@client/assets/sprites/tile.svg';

export default class Tile extends Phaser.GameObjects.Sprite {
  public static spriteName = 'TileSprite';
  public static spriteAsset = sprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Tile.spriteName, Tile.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Tile.spriteName);
    this.setSize(400, 400);
    this.setDisplaySize(400, 400);
    this.setPosition(options.x + 200, options.y + 200);
  }
}
