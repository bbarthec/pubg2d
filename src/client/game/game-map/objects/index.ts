export { default as Shack } from './Shack';
export { default as Tile } from './Tile';
export { default as Tree } from './Tree';
export { default as House } from './House';
export { default as Rock } from './Rock';
export { default as Crate } from './Crate';
