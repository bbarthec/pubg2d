import TorsoSprite from '@client/assets/sprites/torso.svg';

export default class Torso extends Phaser.GameObjects.Sprite {
  public static spriteName = 'TorsoSprite';
  public static spriteAsset = TorsoSprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Torso.spriteName, Torso.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number}) {
    super(scene, options.x, options.y, Torso.spriteName);
    this.displayHeight = 50;
    this.displayWidth = 50;
  }
}
