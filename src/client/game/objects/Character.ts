import Bullet from './Bullet';
import Hand from './Hand';
import Torso from './Torso';
import Weapon from './Weapon';

export interface CharacterOptions {
  x: number;
  y: number;
  rotation: number;
  name: string;
}

export default class Character extends Phaser.GameObjects.Container {
  public static loadSprites(scene: Phaser.Scene) {
    Torso.loadSprite(scene);
    Hand.loadSprite(scene);
    Weapon.loadSprite(scene);
    Bullet.loadSprite(scene);
  }

  protected health: number;
  protected leftHand: Hand;
  protected rightHand: Hand;
  protected weapon: Weapon;
  protected torso: Torso;

  constructor(
    scene: Phaser.Scene,
    options: CharacterOptions,
    physicsType: number = Phaser.Physics.Arcade.STATIC_BODY,
  ) {
    super(scene);
    this.x = options.x;
    this.y = options.y;
    this.rotation = options.rotation;
    this.name = options.name;
    this.setSize(50, 50);
    this.health = 100;

    // construct character from parts
    this.torso = new Torso(this.scene, { x: 0, y: 0 });
    this.add(this.torso);
    this.leftHand = new Hand(this.scene, { x: 20, y: 10 });
    this.add(this.leftHand);
    this.rightHand = new Hand(this.scene, { x: 40, y: -10 });
    this.add(this.rightHand);
    this.weapon = new Weapon(this.scene, { x: 30, y: 0 });
    this.add(this.weapon);

    // add character to scene
    this.scene.add.existing(this);

    // enable physics
    this.scene.physics.world.enable(this, physicsType);
  }

  public getBarrelPosition() {
    // TODO adjust relatively to weapon's barrel
    return {
      x: this.x + Math.cos(this.rotation) * 50,
      y: this.y + Math.sin(this.rotation) * 50,
    };
  }

  public isDead() {
    return this.health <= 0;
  }
}
