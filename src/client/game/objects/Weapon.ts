import WeaponSprite from '@client/assets/sprites/weapon.svg';

export default class Weapon extends Phaser.GameObjects.Sprite {
  public static spriteName = 'WeaponSprite';
  public static spriteAsset = WeaponSprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Weapon.spriteName, Weapon.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number}) {
    super(scene, options.x, options.y, Weapon.spriteName);
    this.displayHeight = 40;
    this.displayWidth = 10;
    this.rotation = Phaser.Math.DegToRad(90);
  }
}
