export default class LifeBar {
  private scene: Phaser.Scene;
  private graphics: Phaser.GameObjects.Graphics;
  private border: Phaser.Geom.Rectangle;
  private fill: Phaser.Geom.Rectangle;

  constructor(scene: Phaser.Scene, { x, y }: { x: number, y: number }) {
    this.scene = scene;
    this.graphics = this.scene.add.graphics({
      fillStyle: { color: 0xFF0000 },
      lineStyle: { width: 2, color: 0x555555 },
    });
    this.graphics.setAlpha(0.5);
    this.border = new Phaser.Geom.Rectangle(x - 2, y + 28, 104, 24);
    this.fill = new Phaser.Geom.Rectangle(x, y + 30, 100, 20);
  }

  public update(x: number, y: number, health: number) {
    this.border.setPosition(x - 2, y + 30);
    this.fill.setPosition(x, y + 30);
    this.fill.setSize(health, 20);

    this.graphics.clear();
    this.graphics.strokeRectShape(this.border);
    this.graphics.fillRectShape(this.fill);
  }
}
