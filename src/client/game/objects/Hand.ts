import HandSprite from '@client/assets/sprites/hand.svg';

export default class Hand extends Phaser.GameObjects.Sprite {
  public static spriteName = 'HandSprite';
  public static spriteAsset = HandSprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Hand.spriteName, Hand.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number}) {
    super(scene, options.x, options.y, Hand.spriteName);
    this.displayWidth = 20;
    this.displayHeight = 20;
  }
}
