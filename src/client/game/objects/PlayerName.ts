export default class PlayerName extends Phaser.GameObjects.Text {
  constructor(scene: Phaser.Scene, position: { x: number, y: number }, playerName: string) {
    super(scene, position.x, position.y, playerName, {
      backgroundColor: 'rgba(0,0,0,0.5)',
      fill: '#ffffff',
      font: '20px Arial',
    });
    this.setOrigin(0, 0);
    this.scene.add.existing(this);
    this.setAlpha(0.7);
  }
}
