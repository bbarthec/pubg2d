import CrosshairSprite from '@client/assets/sprites/crosshair.svg';

export default class Crosshair extends Phaser.GameObjects.Sprite {
  public static spriteName = 'CrosshairSprite';
  public static spriteAsset = CrosshairSprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Crosshair.spriteName, Crosshair.spriteAsset);
  }

  constructor(scene: Phaser.Scene, options: { x: number, y: number }) {
    super(scene, options.x, options.y, Crosshair.spriteName);
    this.setSize(20, 20);
    this.setDisplaySize(20, 20);
    this.setDepth(3);
    scene.add.existing(this);
    scene.physics.world.enable(this);
  }
}
