import _ from 'lodash';

import { getSocket, PlayerData } from '@client/game/socket';
import GameMap from '../game-map';
import { GAME_OVER_SCENE_KEY, GAME_SCENE_KEY } from '../scenes';
import Bullet from './Bullet';
import Character, { CharacterOptions } from './Character';
import Crosshair from './Crosshair';
import LifeBar from './LifeBar';
import PlayerName from './PlayerName';

import ArcadeBody = Phaser.Physics.Arcade.Body;

export default class Player extends Character {
  public static loadSprites(scene: Phaser.Scene) {
    super.loadSprites(scene);
    Crosshair.loadSprite(scene);
  }

  private crosshair: Crosshair;
  private moveKeys: any;
  private maxCrosshairDistance: number = 300;
  private movementAcceleration: number = 1200;
  private maxVelocity: number = 400;
  private drag: number = 1000;
  private bulletsPool: Phaser.Physics.Arcade.Group;
  private enemies: Character[];
  private gameMap: GameMap;
  private playerName: PlayerName;
  private lifeBar: LifeBar;
  private socket: SocketIOClient.Socket;

  constructor(
    scene: Phaser.Scene,
    options: CharacterOptions,
    enemies: Character[],
    gameMap: GameMap,
  ) {
    super(scene, options, Phaser.Physics.Arcade.DYNAMIC_BODY);

    this.socket = getSocket();
    this.socket.on('enemy move', this.moveEnemy);
    this.socket.on('enemy bullet', this.spawnEnemyBullet);
    this.socket.on('hit', this.handleHit);
    this.socket.on('game over', this.gameOver);
    this.enemies = enemies;
    this.gameMap = gameMap;
    this.crosshair = new Crosshair(this.scene, { x: 0, y: 0 });
    this.playerName = new PlayerName(this.scene, { x: 0, y: 20 }, this.name);
    this.lifeBar = new LifeBar(this.scene, { x: this.x, y: this.y });
    this.bulletsPool = this.scene.physics.add.group(undefined, {
      classType: Bullet,
      runChildUpdate: true,
    });

    this.initMouseHandler();
    this.initKeyboardHandler();

    (this.body as ArcadeBody).setDrag(this.drag, this.drag);
    (this.body as ArcadeBody).setCollideWorldBounds(true);
  }

  public updateWithTimes = (time: number, delta: number) => {
    this.setRotation(Phaser.Math.Angle.Between(this.x, this.y, this.crosshair.x, this.crosshair.y));
    (this.crosshair.body as ArcadeBody).setVelocity(
      (this.body as ArcadeBody).velocity.x,
      (this.body as ArcadeBody).velocity.y,
    );

    this.constrainVelocity();
    this.constrainCrosshairPosition();
    this.playerName.setPosition(this.x, this.y);

    this.socket.emit('move', {
      name: this.name,
      rotation: this.rotation,
      x: this.x,
      y: this.y,
    });

    this.lifeBar.update(this.x, this.y, this.health);
  }

  private gameOver = (data: { position: number }) => {
    setTimeout(() => this.scene.scene.start(GAME_OVER_SCENE_KEY, data), 500);
  }

  private moveEnemy = ({ x, y, rotation, name }: PlayerData) => {
    const enemy = this.enemies.find(e => e.name === name);
    if (!enemy) {
      return;
    }
    enemy.setPosition(x, y);
    enemy.setRotation(rotation);
    if (enemy.body) {
      (enemy.body as ArcadeBody).x = x - 25;
      (enemy.body as ArcadeBody).y = y - 25;
    }
  }

  private spawnEnemyBullet = ({ x, y, rotation, xSpeed, ySpeed }: {
    x: number,
    y: number,
    rotation: number,
    xSpeed: number,
    ySpeed: number,
  }) => {
    const bullet = this.bulletsPool.get();
    bullet.setActive(true);
    (bullet as Phaser.Physics.Arcade.Image).setVisible(true);

    if (bullet) {
      (bullet as Bullet).setPosition(x, y);
      (bullet as Bullet).setRotation(rotation);
      (bullet as Bullet).xSpeed = xSpeed;
      (bullet as Bullet).ySpeed = ySpeed;
      this.gameMap.addCollisionWith(bullet, () => bullet.destroy());
      this.enemies.forEach(enemy => this.scene.physics.add.collider(enemy, bullet, () => bullet.destroy()));
      this.scene.physics.add.collider(this, bullet, () => bullet.destroy());
    }
  }

  private handleHit = ({ name, health }: { name: string, health: number }) => {
    if (name === this.name) {
      this.health = health;
    } else if (health === 0) {
      const character = this.enemies.find(e => e.name === name) as Character;
      if (character) {
        character.setVisible(false);
        character.setActive(false);
        (character.body as Phaser.Physics.Arcade.StaticBody).destroy();
        const characterIdx = this.enemies.findIndex(e => e.name === name);
        this.enemies.splice(characterIdx, 1);
      }
    }
  }

  private handleEnemyCollision = (enemy: Phaser.GameObjects.GameObject, bullet: Phaser.GameObjects.GameObject) => {
    this.socket.emit('hit', { name: enemy.name });
    bullet.destroy();
  }

  private fire = () => {
    if (!this.scene.input.mouse.locked || this.isDead()) {
      return;
    }

    const bullet = this.bulletsPool.get();
    bullet.setActive(true);
    (bullet as Phaser.Physics.Arcade.Image).setVisible(true);

    if (bullet) {
      const bulletData = (bullet as Bullet).fire(this, this.crosshair);
      this.socket.emit('bullet', bulletData);
      this.gameMap.addCollisionWith(bullet, () => bullet.destroy());
      this.enemies.forEach(enemy => this.scene.physics.add.collider(enemy, bullet, this.handleEnemyCollision));
    }
  }

  private constrainVelocity() {
    const { x, y } = (this.body as ArcadeBody).velocity;
    if (x ** 2 + y ** 2 > this.maxVelocity ** 2) {
      const angle = Math.atan2(y, x);
      (this.body as ArcadeBody).velocity.setTo(
        Math.cos(angle) * this.maxVelocity,
        Math.sin(angle) * this.maxVelocity,
      );
    }
  }

  private constrainCrosshairPosition() {
    const distX = this.crosshair.x - this.x;
    const distY = this.crosshair.y - this.y;

    if (distX ** 2 + distY ** 2 > this.maxCrosshairDistance ** 2) {
      const angle = Math.atan2(distY, distX);
      this.crosshair.setPosition(
        this.x + Math.cos(angle) * this.maxCrosshairDistance,
        this.y + Math.sin(angle) * this.maxCrosshairDistance,
      );
    }
  }

  private initMouseHandler() {
    this.scene.scene.systems.game.canvas.addEventListener('mousedown', () => {
      this.scene.scene.systems.game.input.mouse.requestPointerLock();
    });

    this.scene.input.on(
      'pointermove',
      (pointer: Phaser.Input.Pointer) => {
        if (this.scene.input.mouse.locked && !this.isDead()) {
          this.crosshair.x += pointer.movementX;
          this.crosshair.y += pointer.movementY;
        }
      },
      this.scene,
    );

    this.scene.input.on(
      'pointerdown',
      this.fire,
      this.scene,
    );
  }

  private initKeyboardHandler() {
    this.moveKeys = this.scene.input.keyboard.addKeys({
      down: Phaser.Input.Keyboard.KeyCodes.S,
      left: Phaser.Input.Keyboard.KeyCodes.A,
      right: Phaser.Input.Keyboard.KeyCodes.D,
      up: Phaser.Input.Keyboard.KeyCodes.W,
    });

    this.scene.input.keyboard.on('keydown_W', () => {
      if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationY(-1 * this.movementAcceleration);
    });
    this.scene.input.keyboard.on('keydown_S', () => {
      if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationY(1 * this.movementAcceleration);
    });
    this.scene.input.keyboard.on('keydown_A', () => {
      if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationX(-1 * this.movementAcceleration);
    });
    this.scene.input.keyboard.on('keydown_D', () => {
      if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationX(1 * this.movementAcceleration);
    });

    this.scene.input.keyboard.on('keyup_W', () => {
      if (this.moveKeys.down.isUp) {
        if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationY(0);
      }
    });
    this.scene.input.keyboard.on('keyup_S', () => {
      if (this.moveKeys.up.isUp) {
        if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationY(0);
      }
    });
    this.scene.input.keyboard.on('keyup_A', () => {
      if (this.moveKeys.right.isUp) {
        if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationX(0);
      }
    });
    this.scene.input.keyboard.on('keyup_D', () => {
      if (this.moveKeys.left.isUp) {
        if (this.body && !this.isDead()) (this.body as ArcadeBody).setAccelerationX(0);
      }
    });
  }
}
