import BulletSprite from '@client/assets/sprites/bullet.svg';
import Character from './Character';

export default class Bullet extends Phaser.GameObjects.Image {
  public static spriteName = 'BulletSprite';
  public static spriteAsset = BulletSprite;

  public static loadSprite(scene: Phaser.Scene) {
    scene.load.image(Bullet.spriteName, Bullet.spriteAsset);
  }

  public xSpeed = 0;
  public ySpeed = 0;

  private speed = 1;
  private born = 0;
  private direction = 0;
  private maxLifespan = 500;

  constructor(scene: Phaser.Scene) {
    super(scene, 0, 0, Bullet.spriteName);
    this.setSize(10, 10);
    this.setDisplaySize(6, 12);
  }

  public fire(shooter: Character, target: { x: number, y: number }) {
    const { x: startingX, y: startingY } = shooter.getBarrelPosition();
    const { x, y } = shooter;
    this.setPosition(startingX, startingY);
    this.direction = Math.atan((target.x - x) / (target.y - y));

    if (target.y >= y) {
      this.xSpeed = this.speed * Math.sin(this.direction);
      this.ySpeed = this.speed * Math.cos(this.direction);
    } else {
      this.xSpeed = -this.speed * Math.sin(this.direction);
      this.ySpeed = -this.speed * Math.cos(this.direction);
    }

    this.rotation = shooter.rotation + Phaser.Math.DegToRad(90);
    this.born = 0;
    return {
      rotation: this.rotation,
      x: this.x,
      xSpeed: this.xSpeed,
      y: this.y,
      ySpeed: this.ySpeed,
    };
  }

  public update() {
    // Walk around for compiler errors and mismatch in .d.ts file
    // This is available as we're using Bullet from ObjectPool
    const time: number = arguments[0];
    const delta: number = arguments[1];
    this.x += this.xSpeed * delta;
    this.y += this.ySpeed * delta;
    this.born += delta;
    if (this.born > this.maxLifespan) {
      this.destroy();
    }
  }
}
