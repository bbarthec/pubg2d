import {
  GameOverScene,
  GameScene,
  LoadingScene,
  RejectScene,
} from './scenes';

const config: GameConfig = {
  backgroundColor: 'F0F0F0',
  height: window.innerHeight,
  input: {
    keyboard: true,
  },
  parent: 'game',
  physics: {
    arcade: {
      debug: false,
      gravity: { y: 0 },
    },
    default: 'arcade',
  },
  scene: [LoadingScene, GameScene, RejectScene, GameOverScene],
  title: 'Game',
  type: Phaser.WEBGL,
  width: window.innerWidth,
};

export default class Game extends Phaser.Game {
  public handleRestart: () => void;

  constructor(playerName: string, handleRestart: () => void) {
    super(config);
    localStorage.setItem('playerName', playerName);
    this.handleRestart = handleRestart;
  }

  public resize(width: number, height: number) {
    const a = {};
  }
}
