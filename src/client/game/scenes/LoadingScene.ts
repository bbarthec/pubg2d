import {
  GAME_SCENE_KEY,
  LOADING_SCENE_KEY,
  REJECT_SCENE_KEY,
} from '@client/game/scenes';

import { initSocket, PlayerData } from '../socket';

import BackgroundImage from '@client/assets/images/background.png';

export default class GameScene extends Phaser.Scene {
  private timeout?: number;
  private text: Phaser.GameObjects.Text;
  private background: Phaser.GameObjects.Image;
  private socket: SocketIOClient.Socket;

  constructor() {
    super({ key: LOADING_SCENE_KEY });
  }

  public preload() {
    this.load.image('backgroundImage', BackgroundImage);
  }

  public create() {
    const playerName = localStorage.getItem('playerName')!;
    this.data.set('playerName', playerName);
    this.background = this.add.image(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      'backgroundImage');
    this.background.setAlpha(0.5);
    this.text = this.add.text(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      [
        `Player '${this.data.get('playerName')}'`,
        'Waiting for more players...',
      ],
      {
        align: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
        fill: '#ffffff',
        font: '20px monospace',
      });

    this.text.setPadding(20, 20, 20, 20).setOrigin(0.5, 0.5);
    this.socket = initSocket(playerName, {
      onConfig: this.handleConfig,
      onGameStart: this.handleGameStart,
      onLobbyTick: this.handleLobbyTick,
      onNewPlayer: this.handleNewPlayer,
      onSessionRejected: this.handleSessionRejection,
    });
  }

  public update(time: number, delta: number) {
    this.text.setText([
      `Player '${this.data.get('playerName')}'`,
      'Loading...',
      !!this.timeout && `${this.timeout} seconds left ...`,
    ].filter(<T>(n: T | boolean): n is T => !!n));
  }

  private handleSessionRejection = () => {
    this.scene.start(REJECT_SCENE_KEY, this.data);
  }

  private handleLobbyTick = ({ time }: { time: number }) => {
    this.timeout = time;
  }

  private handleGameStart = () => {
    this.scene.start(GAME_SCENE_KEY, this.data);
  }

  private handleConfig = ({
    playerRotation,
    playerX,
    playerY,
    worldWidth,
    worldHeight,
    players,
  }: {
    playerX: number,
    playerY: number,
    playerRotation: number,
    worldWidth: number,
    worldHeight: number,
    players: PlayerData[],
  }) => {
    this.data.set('playerX', playerX);
    this.data.set('playerY', playerY);
    this.data.set('playerRotation', playerRotation);
    this.data.set('worldWidth', worldWidth);
    this.data.set('worldHeight', worldHeight);
    this.data.set('enemies', players);
  }

  private handleNewPlayer = (player: PlayerData) => {
    const actualEnemies = this.data.get('enemies') as PlayerData[] || [];
    this.data.set('enemies', [...actualEnemies, player]);
  }
}
