import {
  GAME_OVER_SCENE_KEY,
} from '.';
import Game from '../';

import BackgroundImage from '@client/assets/images/background.png';

export default class GameScene extends Phaser.Scene {
  private text: Phaser.GameObjects.Text;
  private background: Phaser.GameObjects.Image;
  private position: number;
  private keyEnter: Phaser.Input.Keyboard.Key;

  constructor() {
    super({ key: GAME_OVER_SCENE_KEY });
  }

  public preload() {
    this.load.image('backgroundImage', BackgroundImage);
  }

  public init(data: { position: number }) {
    this.position = data.position!;
  }

  public create() {
    this.background = this.add.image(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      'backgroundImage');
    this.background.setAlpha(0.5);
    const texts = this.position === 1 ? [
      'WINNER WINNER',
      'CHICKEN DINNER',
      '',
      'HIT "ENTER" TO RESTART',
    ] : [
      'GAME OVER...',
      'BETTER LUCK NEXT TIME',
      `RANK #${this.position}`,
      '',
      'HIT "ENTER" TO RESTART',
    ];

    this.text = this.add.text(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      texts,
      {
        align: 'center',
        backgroundColor: this.position === 1 ? 'rgba(30,200,30,0.6)' : 'rgba(200, 30, 30, 0.6)',
        fill: '#ffffff',
        font: '20px monospace',
      });

    this.text.setPadding(20, 20, 20, 20).setOrigin(0.5, 0.5);
    this.keyEnter = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.ENTER);
  }

  public update() {
    if (this.keyEnter.isDown) {
      (this.scene.scene.sys.game as Game).handleRestart();
    }
  }
}
