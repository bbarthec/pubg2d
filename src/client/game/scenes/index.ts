export { default as GameScene } from './GameScene';
export { default as GameOverScene } from './GameOverScene';
export { default as LoadingScene } from './LoadingScene';
export { default as RejectScene } from './RejectScene';

export const LOADING_SCENE_KEY = 'LoadingScene';
export const GAME_OVER_SCENE_KEY = 'GameOverScene';
export const GAME_SCENE_KEY = 'GameScene';
export const REJECT_SCENE_KEY = 'RejectScene';
