import _ from 'lodash';

import GameMap from '@client/game/game-map';
import { GAME_SCENE_KEY } from '@client/game/scenes';
import { PlayerData } from '@client/game/socket';
import {
  Character,
  Player,
} from '../objects';

export default class GameScene extends Phaser.Scene {
  private gameHeight: number;
  private gameWidth: number;
  private player: Player;
  private enemies: Character[];
  private gameMap: GameMap;
  private debugText: Phaser.GameObjects.Text;

  constructor() {
    super({ key: GAME_SCENE_KEY });
  }

  public preload() {
    GameMap.loadSprites(this);
    Player.loadSprites(this);
  }

  public init(data: Phaser.Data.DataManagerPlugin) {
    _.forOwn(data.getAll(), (value, key) => this.data.set(key, value));
  }

  public create() {
    this.gameWidth = this.data.get('worldWidth') as number;
    this.gameHeight = this.data.get('worldHeight') as number;
    this.physics.world.setBounds(0, 0, this.gameWidth, this.gameHeight);
    this.gameMap = new GameMap(this);

    this.enemies = (this.data.get('enemies') as PlayerData[]).map(
      options => new Character(this, options));
    this.player = new Player(
      this,
      {
        name: this.data.get('playerName'),
        rotation: this.data.get('playerRotation'),
        x: this.data.get('playerX'),
        y: this.data.get('playerY'),
      },
      this.enemies,
      this.gameMap);

    this.enemies.forEach(e => this.physics.add.collider(this.player, e));
    this.gameMap.addCollisionWith(this.player);

    this.cameras.main.startFollow(this.player);

    this.debugText = this.add.text(16, 16, '', {
      backgroundColor: '#000000',
      fill: '#ffffff',
      font: '20px Arial',
    });
    this.debugText.setScrollFactor(0);
    this.debugText.setDepth(2);
  }

  public update(time: number, delta: number) {
    this.player.updateWithTimes(time, delta);

    const { x, y } = (
      this.input.activePointer.positionToCamera(this.cameras.main) as { x: number, y: number });
    this.debugText.setText([
      `Position: ${x}, ${y}`,
      `FPS: ${this.scene.systems.game.loop.actualFps}`,
    ]);
  }
}
