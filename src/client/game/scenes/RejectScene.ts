import {
  REJECT_SCENE_KEY,
} from '.';

import BackgroundImage from '@client/assets/images/background.png';

export default class GameScene extends Phaser.Scene {
  private text: Phaser.GameObjects.Text;
  private background: Phaser.GameObjects.Image;

  constructor() {
    super({ key: REJECT_SCENE_KEY });
  }

  public preload() {
    this.load.image('backgroundImage', BackgroundImage);
  }

  public create() {
    this.background = this.add.image(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      'backgroundImage');
    this.background.setAlpha(0.5);
    this.text = this.add.text(
      this.cameras.main.x + this.cameras.main.width / 2,
      this.cameras.main.y + this.cameras.main.height / 2,
      [
        `Player '${this.data.get('playerName')}'`,
        'Rejection!',
      ],
      {
        align: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
        fill: '#ffffff',
        font: '20px monospace',
      });

    this.text.setPadding(20, 20, 20, 20).setOrigin(0.5, 0.5);
  }
}
