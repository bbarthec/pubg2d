const path = require('path');
const fs = require('fs');
const config = require('config');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin')

const phaser = path.join(__dirname, '/node_modules/phaser/dist/phaser.js');

const distDir = path.resolve(__dirname, 'dist');
const buildDir = path.resolve(distDir,'public');
if (!fs.existsSync(distDir)) {
  fs.mkdirSync(distDir);
}
if (!fs.existsSync(buildDir)) {
  fs.mkdirSync(buildDir);
}
// publish to client only public section of config
fs.writeFileSync(
  path.resolve(buildDir, 'clientConfig.json'),
  JSON.stringify({ client: config.get('client') }),
);

const plugins = [
  new HtmlWebpackPlugin({
    inject: 'body',
    template: path.resolve(__dirname, 'src/client/template.html'),
    filename: 'index.html',
    favicon: path.resolve(__dirname, 'src/client/assets/favicon.ico'),
    chunks: ['phaser', 'vendors', 'game'],
  }),
  new webpack.EnvironmentPlugin({
    NODE_ENV: 'development',
  }),
  new webpack.DefinePlugin({
    WEBGL_RENDERER: true,
    CANVAS_RENDERER: false,
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NamedModulesPlugin(),
  new ProgressBarPlugin(),
];

const rules = [
  { test: /\.(tsx|ts)?$/,
    use: [
      { loader: 'babel-loader' },
      { loader: 'awesome-typescript-loader',
        options: {
          configFileName: path.resolve(__dirname, 'src/client/tsconfig.json'),
        },
      },
    ],
  },
  { test: /\.scss$/,
    use: [
      { loader: 'style-loader' },
      { loader: 'css-loader',
        options: {
          autoprefixer: false,
        },
      },
      { loader: 'sass-loader' },
    ],
  },
  { test: /sprites\/.+\.(gif|png|jpe?g|svg)$/i,
    use: {
      loader: 'file-loader',
      options: {
        name: 'assets/sprites/[name].[ext]',
      },
    },
  },
  { test: /images\/.+\.(gif|png|jpe?g|svg)$/i,
    use: {
      loader: 'file-loader',
      options: {
        name: 'assets/images/[name].[ext]',
      },
    },
  },
  { test: /\.fnt/i,
    use: {
      loader: 'file-loader',
      options: {
        name: 'assets/fonts/[name].[ext]',
      },
    },
  },
  { test: /phaser\.js$/,
    use: {
      loader: 'expose-loader',
      options: 'Phaser',
    },
  },
  { test: [/\.vert$/, /\.frag$/],
    use: {
      loader: 'raw-loader',
    },
  },
];

module.exports = {
  devtool: 'inline-source-map',
  mode: 'development',
  target: 'web',
  optimization: {
    splitChunks: {
      cacheGroups: {
        commons: {
          test: /\/node_modules\/(?!phaser).*/,
          name: "vendors",
          chunks: "all",
        },
        phaser: {
          test: /\/node_modules\/phaser/,
          name: "phaser",
          chunks: "all",
        },
      }
    }
  },
  entry: {
    phaser: phaser,
    game: path.resolve(__dirname, 'src/client/index.tsx'),
  },
  output: {
    path: buildDir,
    publicPath: '/',
    filename: '[name].js',
  },
  module: {
    rules,
  },
  plugins,
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
    modules: [
      path.resolve(__dirname, 'src/client'),
      path.resolve(__dirname, 'node_modules'),
    ],
    alias: {
      config: path.resolve(buildDir, 'clientConfig.json'),
      '@client': path.resolve(__dirname, 'src/client')
    },
  },
  devServer: {
    port: config.get('server.webpack.dev.port'),
    hot: true,
    publicPath: '/',
    historyApiFallback: true,
    stats: {
      colors: true,
      assets: true,
      children: false,
      chunks: false,
      excludeAssets: name => !['js(.gz)?', 'css(.gz)?', 'html'].some(ext => RegExp(`.*${ext}$`).test(name)),
      hash: false,
      modules: false,
      timings: true,
      version: false,
      warnings: true,
    },
  },
};
