# PUBG 2D

## Usage

Only development currently is available.

All needed to build and launch this project:

1. install `node.js` (it comes with `npm` package manager)
2. `npm i -g yarn` (you can omit it `yarn` is already on your machine)
3. `yarn` - that would install all dependencies
4. `yarn run watch` - that would launch dev environment

## Project layout

It consist of two parts: `client` and `server`.

`yarn run watch` would run three concurrent tasks:

* `watch-webpack` - task for building client side application
* `watch-ts` - task for compiling typescript on server
* `watch-node` - task for launching server

## Current development

Use only client side bundle building: `npm run watch-webpack`
